﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Windows.Forms;
using static SharpChip8.Chip.GraphicCard;

namespace SharpChip8.Chip
{
	public class System
	{
		public CPU Cpu;
		public Keyboard KeyBoard;
		private Stopwatch Clock;
		private long OpCodePerSecond = 600;
		public string romPath;
		public bool loaded;
		public bool IsInit { get; private set; }
		public bool IsStarted { get; private set; }

		public System()
		{
			Cpu = new CPU();
			KeyBoard = new Keyboard();
			KeyBoard.SetKey(Keys.D1, 1);
			KeyBoard.SetKey(Keys.D2, 2);
			KeyBoard.SetKey(Keys.D3, 3);
			KeyBoard.SetKey(Keys.D4, 12);

			KeyBoard.SetKey(Keys.A, 4);
			KeyBoard.SetKey(Keys.Z, 5);
			KeyBoard.SetKey(Keys.E, 6);
			KeyBoard.SetKey(Keys.R, 13);

			KeyBoard.SetKey(Keys.Q, 7);
			KeyBoard.SetKey(Keys.S, 8);
			KeyBoard.SetKey(Keys.D, 9);
			KeyBoard.SetKey(Keys.F, 14);

			KeyBoard.SetKey(Keys.W, 10);
			KeyBoard.SetKey(Keys.X, 0);
			KeyBoard.SetKey(Keys.C, 11);
			KeyBoard.SetKey(Keys.F, 15);
			Clock = new Stopwatch();
		}

		public void Init()
		{
			Cpu.Reset();
			loaded = false;
			IsInit = true;
		}

		public void Start()
		{
			if (!Clock.IsRunning && loaded)
			{
				while (IsStarted) {
					Thread.Sleep(10);
				}
				IsStarted = true;
				Console.WriteLine("Started");
				Cpu.Reset();
				Clock.Start();
				int opCodeCounter = 0;
				Cpu.ProgramCounter = 0x200;
				while (Clock.IsRunning)
				{
					ushort opCode = Cpu.GetOpCode();
					Cpu.ExcuteOpCode(opCode, KeyBoard);
					opCodeCounter++;
					Thread.Sleep(16);
				}
			}
			IsStarted = false;
			Console.WriteLine("Stopped");
		}

		public void Stop()
		{
			Clock.Stop();
		}
		public void LoadROM(string path)
		{
			Stop();
			byte[] buffer = File.ReadAllBytes(path);
			for (int i=0;i< buffer.Length; i++){
				Cpu.Memory[i+0x200] = buffer[i];
			}
			loaded = true;
			romPath = path;
		}
	}
}
