﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SharpChip8.Chip
{
	public class Keyboard
	{
		private const int KeysNumber = 16;
		public Key[] Keys = new Key[KeysNumber];
		public class Key
		{
			public Keys Code;
			public byte Id;
			public bool IsPressed = false;
		}

		public void SetKey(Keys code, byte id)
		{
			if (id < KeysNumber)
			{
				Keys[id] = new Key() { Code = code, Id = id };
			}
		}

		public bool GetState(byte id)
		{
			if (id < KeysNumber)
			{
				return Keys[id].IsPressed;
			}
			return false;
		}
	}

}
