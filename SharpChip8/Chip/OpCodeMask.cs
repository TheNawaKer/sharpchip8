﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharpChip8.Chip
{
	public static class OpCodeMask
	{
		public static ushort[] Mask;
		public static ushort[] Id;

		public static string[] OpCode = new string[] {
			"0NNN",
			"00E0",
			"00EE",
			"1NNN",
			"2NNN",
			"3XNN",
			"4XNN",
			"5XY0",
			"6XNN",
			"7XNN",
			"8XY0",
			"8XY1",
			"8XY2",
			"BXY3",
			"8XY4",
			"8XY5",
			"8XY6",
			"8XY7",
			"8XYE",
			"9XY0",
			"ANNN",
			"BNNN",
			"CXNN",
			"DXYN",
			"EX9E",
			"EXA1",
			"FX07",
			"FX0A",
			"FX15",
			"FX18",
			"FX1E",
			"FX29",
			"FX33",
			"FX55",
			"FX65"
		};

		static OpCodeMask()
		{
			Mask = new ushort[OpCode.Length];
			Id = new ushort[OpCode.Length];


			Mask[0] = 0x0000; Id[0] = 0x0FFF;          /* 0NNN */
			Mask[1] = 0xFFFF; Id[1] = 0x00E0;          /* 00E0 */
			Mask[2] = 0xFFFF; Id[2] = 0x00EE;          /* 00EE */
			Mask[3] = 0xF000; Id[3] = 0x1000;          /* 1NNN */
			Mask[4] = 0xF000; Id[4] = 0x2000;          /* 2NNN */
			Mask[5] = 0xF000; Id[5] = 0x3000;          /* 3XNN */
			Mask[6] = 0xF000; Id[6] = 0x4000;          /* 4XNN */
			Mask[7] = 0xF00F; Id[7] = 0x5000;          /* 5XY0 */
			Mask[8] = 0xF000; Id[8] = 0x6000;          /* 6XNN */
			Mask[9] = 0xF000; Id[9] = 0x7000;          /* 7XNN */
			Mask[10] = 0xF00F; Id[10] = 0x8000;          /* 8XY0 */
			Mask[11] = 0xF00F; Id[11] = 0x8001;          /* 8XY1 */
			Mask[12] = 0xF00F; Id[12] = 0x8002;          /* 8XY2 */
			Mask[13] = 0xF00F; Id[13] = 0x8003;          /* BXY3 */
			Mask[14] = 0xF00F; Id[14] = 0x8004;          /* 8XY4 */
			Mask[15] = 0xF00F; Id[15] = 0x8005;          /* 8XY5 */
			Mask[16] = 0xF00F; Id[16] = 0x8006;          /* 8XY6 */
			Mask[17] = 0xF00F; Id[17] = 0x8007;          /* 8XY7 */
			Mask[18] = 0xF00F; Id[18] = 0x800E;          /* 8XYE */
			Mask[19] = 0xF00F; Id[19] = 0x9000;          /* 9XY0 */
			Mask[20] = 0xF000; Id[20] = 0xA000;          /* ANNN */
			Mask[21] = 0xF000; Id[21] = 0xB000;          /* BNNN */
			Mask[22] = 0xF000; Id[22] = 0xC000;          /* CXNN */
			Mask[23] = 0xF000; Id[23] = 0xD000;          /* DXYN */
			Mask[24] = 0xF0FF; Id[24] = 0xE09E;          /* EX9E */
			Mask[25] = 0xF0FF; Id[25] = 0xE0A1;          /* EXA1 */
			Mask[26] = 0xF0FF; Id[26] = 0xF007;          /* FX07 */
			Mask[27] = 0xF0FF; Id[27] = 0xF00A;          /* FX0A */
			Mask[28] = 0xF0FF; Id[28] = 0xF015;          /* FX15 */
			Mask[29] = 0xF0FF; Id[29] = 0xF018;          /* FX18 */
			Mask[30] = 0xF0FF; Id[30] = 0xF01E;          /* FX1E */
			Mask[31] = 0xF0FF; Id[31] = 0xF029;          /* FX29 */
			Mask[32] = 0xF0FF; Id[32] = 0xF033;          /* FX33 */
			Mask[33] = 0xF0FF; Id[33] = 0xF055;          /* FX55 */
			Mask[34] = 0xF0FF; Id[34] = 0xF065;          /* FX65 */
		}

		public static string OpCodeNum(ushort opcode)
		{
			for(byte i =0;i < OpCode.Length; i++)
			{
				ushort id = (ushort)(Mask[i] & opcode);
				if (id == Id[i])
					return OpCode[i];
			}
			throw new InvalidOperationException("Unknown OpCode "+opcode.ToString("X"));
		}
	}
}
