﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static SharpChip8.Chip.Keyboard;

namespace SharpChip8.Chip
{
	public class CPU
	{
		private const int MemorySize = 4096;
		private const int StackSize = 16;
		private const int RegistreNumber = 16;

		/// <summary>
		/// Mémoire
		/// </summary>
		public byte[] Memory = new byte[MemorySize];

		/// <summary>
		/// Compteur d'opération
		/// </summary>
		public ushort ProgramCounter;
		/// <summary>
		/// nombre de saut. utilisé notamment lors de l'exectution d'une fonction
		/// </summary>
		public ushort[] Stack;
		/// <summary>
		/// position actuel dans la stack
		/// </summary>
		public byte StackPos;

		/// <summary>
		/// Registre de calcul allant de V0 à VF
		/// </summary>
		public byte[] V;
		/// <summary>
		/// Registre d'adresse
		/// </summary>
		public ushort I;

		/// <summary>
		/// Horloge interne (60Hz)
		/// </summary>
		public byte Clock;
		/// <summary>
		/// Horloge Sonore
		/// </summary>
		public byte SoundClock;

		public GraphicCard Screen = new GraphicCard();
		Random rnd = new Random();

		public void Reset()
		{
			ProgramCounter = 0;
			Stack = new ushort[StackSize];
			StackPos = 0;
			V = new byte[RegistreNumber];
			I = 0;
			Clock = 0;
			SoundClock = 0;
			Screen.Init();
		}

		public ushort GetOpCode()
		{
			return (ushort)(Memory[ProgramCounter] << 8 | Memory[ProgramCounter + 1]);
		}

		public void ExcuteOpCode(ushort opCode, Keyboard keys)
		{
			bool noJump = false;
			string id = OpCodeMask.OpCodeNum(opCode);
			Console.WriteLine(id + " "+opCode.ToString("X"));
			switch (id)
			{
				case "0NNN":
					break;
				case "00E0": // efface l'écran
					Screen.Clear();
					break;
				case "00EE": // retour d'un saut
					if (StackPos > 0)
					{
						StackPos -= 1;
						ProgramCounter = Stack[StackPos];
					}
					else
						throw new InsufficientExecutionStackException();
					break;
				case "1NNN": // effectue un saut
					ProgramCounter = GetNNN(opCode);
					noJump = true;
					break;
				case "2NNN": // appel NNN
					if (StackPos < StackSize)
					{
						Stack[StackPos] = ProgramCounter;
						StackPos += 1;
						ProgramCounter = GetNNN(opCode);
						noJump = true;
					}
					else
						throw new StackOverflowException();
					break;

				case "3XNN": // saute l'instruction suivante si V[x] == NN
					if (V[GetX(opCode)] == GetNN(opCode))
						ProgramCounter += 2;
					break;
				case "4XNN": // saute l'instruction suivante si V[x] != NN
					if (V[GetX(opCode)] != GetNN(opCode))
						ProgramCounter += 2;
					break;
				case "5XY0": // saute l'instruction suivante si V[x] == V[y]
					if (V[GetX(opCode)] == V[GetY(opCode)])
						ProgramCounter += 2;
					break;
				case "6XNN": // V[x] = NN
					V[GetX(opCode)] = GetNN(opCode);
					break;
				case "7XNN": // V[x] += NN
					V[GetX(opCode)] += GetNN(opCode);
					break;
				case "8XY0": // V[x] = V[y]
					V[GetX(opCode)] = V[GetY(opCode)];
					break;
				case "8XY1": // V[x] = V[x] OR V[y]
					V[GetX(opCode)] = (byte)(V[GetX(opCode)] | V[GetY(opCode)]);
					break;
				case "8XY2":// V[x] = V[x] AND V[y]
					V[GetX(opCode)] = (byte)(V[GetX(opCode)] & V[GetY(opCode)]);
					break;
				case "BXY3":// V[x] = V[x] XOR V[y]
					V[GetX(opCode)] = (byte)(V[GetX(opCode)] ^ V[GetY(opCode)]);
					break;
				case "8XY4":// V[x] += V[y]; si dépassement mémoire alors VF = 1, 0 sinon
					V[0xF] = 0;
					ushort res = (ushort)(V[GetX(opCode)] + V[GetY(opCode)]);
					if (res > 0xFF)
						V[0xF] = 1;
					V[GetX(opCode)] = (byte)res;
					break;
				case "8XY5":// V[x] -= V[y]; si emprunt alors VF = 1, 0 sinon
					V[0xF] = 0;
					if (V[GetX(opCode)] < V[GetY(opCode)])
						V[0xF] = 1;
					V[GetX(opCode)] -= V[GetY(opCode)];
					break;
				case "8XY6": // Décallage droit de V[x] d'un bit; VF = bit de poids faible de V[x] avant décallage
					V[0xF] = (byte)(GetBit(V[GetX(opCode)], 7) ? 1 : 0);
					V[GetX(opCode)] = (byte)(V[GetX(opCode)] >> 1);
					break;
				case "8XY7": //  V[x] = V[y] - V[x]. VF = 1 si emprunt
					V[0xF] = 0;
					if (V[GetX(opCode)] > V[GetY(opCode)])
						V[0xF] = 1;
					else
						V[GetX(opCode)] = (byte)(V[GetY(opCode)] - V[GetX(opCode)]);
					break;
				case "8XYE": // Décallage gauche de V[x] d'un bit; VF = bit de poids fort de V[x] avant décallage
					V[0xF] = (byte)(GetBit(V[GetX(opCode)], 0) ? 1 : 0);
					V[GetX(opCode)] = (byte)(V[GetX(opCode)] << 1);
					break;
				case "9XY0": // saute l'instruction suivante si V[x] != V[y]
					if (V[GetX(opCode)] != V[GetY(opCode)])
						ProgramCounter += 2;
					break;
				case "ANNN": // I = NNN
					I = GetNNN(opCode);
					break;
				case "BNNN": // passe à NNN + V0
					ProgramCounter = (ushort)(GetNNN(opCode) + V[0]);
					noJump = true;
					break;
				case "CXNN": // V[x] = aleatoire < NN
					V[GetX(opCode)] = (byte)rnd.Next(0, GetNN(opCode) - 1);
					break;
				case "DXYN": // Dessine un sprite en V[x] V[y]
					ushort x = GetX(opCode);
					ushort y = GetY(opCode);
					ushort height = GetN(opCode);
					V[0xF] = 0;
					for (byte i = 0; i < height; i++)
					{
						byte row = Memory[I + i];
						int posy = (V[y] + i) % GraphicCard.Height;
						for (byte j = 0; j < 8; j++)
						{
							int posx = (V[x] + j) % GraphicCard.Width;
							bool bit = GetBit(row, j);
							if (Screen.Pixels[posx, posy].Color == Color.White && bit) // pixel déja allumé
							{
								V[0xF] = 1;
								Screen.Pixels[posx, posy].Color = Color.Black;
							}
							else
								Screen.Pixels[posx, posy].Color = bit ? Color.White : Color.Black;
						}
					}
					Screen.DrawScreen();
					break;
				case "EX9E": // saute l'instruction suivante si la touche V[x] est pressée
					if(keys.GetState(V[GetX(opCode)]))
						ProgramCounter += 2;
					break;
				case "EXA1": // saute l'instruction suivante si la touche V[x] n'est pressée
					if (!keys.GetState(V[GetX(opCode)]))
						ProgramCounter += 2;
					break;
				case "FX07": // V[x] = Clock
					V[GetX(opCode)] = Clock;
					break;
				case "FX0A": // attend appuie d'une touche et stock la valeur dans V[x]
					Key key = null;
					do
					{
						key = keys.Keys.FirstOrDefault(k => k.IsPressed);
					} while (key == null);
					V[GetX(opCode)] = key.Id;
					break;
				case "FX15": // Clock = V[x]
					Clock = V[GetX(opCode)];
					break;
				case "FX18": // SoundClock = V[x]
					SoundClock = V[GetX(opCode)];
					break;
				case "FX1E": // V[x] += I; si overflow alors VF = 1, 0 sinon
					V[0xF] = 0;
					int calcul = I + V[GetX(opCode)];
					V[GetX(opCode)] = (byte)calcul;
					if (calcul > 0xFF)
						V[0xF] = 1;
					break;
				case "FX29": // Positionne I sur le sprite de la lettre ou du chiffre en mémoire (la police fait 4x5)
					I = (ushort)(5 * V[GetX(opCode)]);
					break;
				case "FX33": // decimal(V[x]) => Memory[I], Memory[I+1] et Memory[I+2]
					Memory[I] = (byte)((V[GetX(opCode)] - V[GetX(opCode)] % 100) / 100);
					Memory[I + 1] = (byte)((((V[GetX(opCode)] - V[GetX(opCode)] % 10) / 10) % 10));
					Memory[I + 2] = (byte)(V[GetX(opCode)] - Memory[I] * 100 - 10 * Memory[I + 1]);
					break;
				case "FX55": // V[0-x] => Memory depuis I
					for (int i = 0; i <= GetX(opCode); i++)
					{
						Memory[I + i] = V[i];
					}
					break;
				case "FX65": // V[0-x] <= Memory depuis I
					for (int i = 0; i <= GetX(opCode); i++)
					{
						V[i] = Memory[I + i];
					}
					break;
			}
			if (!noJump)
				ProgramCounter += 2;
		}

		public bool GetBit(byte data, byte pos)
		{
			return ((data) & (0x1 << 7 - pos)) != 0;
		}

		#region OpCodeGetter

		public byte GetX(ushort opCode)
		{
			return (byte)((opCode & (0x0F00)) >> 8);
		}
		public byte GetY(ushort opCode)
		{
			return (byte)((opCode & (0x00F0)) >> 4);
		}
		public byte GetNN(ushort opCode)
		{
			return (byte)((opCode & (0x00FF)));
		}
		public byte GetN(ushort opCode)
		{
			return (byte)(opCode & (0x000F));
		}
		public ushort GetNNN(ushort opCode)
		{
			return (ushort)(opCode & (0x0FFF));
		}

		#endregion OpCodeGetter
	}
}
