﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharpChip8.Chip
{
	public class GraphicCard
	{
		public delegate void DrawEventHandler();
		public event DrawEventHandler Draw;

		public const int Width = 64;
		public const int Height = 32;

		/// <summary>
		/// Tableau de pixel représentant les différents pixels affichés
		/// </summary>
		public Pixel[,] Pixels;


		public class Pixel
		{
			public int PosX;
			public int PosY;
			public Color Color;
		}

		public GraphicCard() {
			Pixels = new Pixel[Width, Height];
		}

		public void Init()
		{
			for (int x = 0; x < Pixels.GetLength(0); x++)
			{
				for (int y = 0; y < Pixels.GetLength(1); y++)
				{
					Pixels[x, y] = new Pixel() { PosX = x, PosY = y, Color = Color.Black };
				}
			}
			DrawScreen();
		}

		public void Clear()
		{
			for (int x = 0; x < Pixels.GetLength(0); x++)
			{
				for (int y = 0; y < Pixels.GetLength(1); y++)
				{
					Pixels[x, y].Color = Color.Black;
				}
			}
			DrawScreen();
		}

		public void DrawScreen()
		{
			Draw?.Invoke();
		}
	}
}
