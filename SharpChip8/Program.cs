﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SharpChip8
{
	static class Program
	{
		public static Window myWindow;
		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main()
		{
			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);

			Chip.System sys = new Chip.System();
			myWindow = new Window();
			myWindow.SetScreen(sys);
			sys.Init();

			Application.Run(myWindow);
		}

		public static void RunOnUIThread(Action action)
		{
			if (myWindow.InvokeRequired)
				myWindow.Invoke(action);
			else
				action();
		}
	}
}
