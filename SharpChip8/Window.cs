﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;

using static SharpChip8.Chip.GraphicCard;
using static SharpChip8.Chip.Keyboard;

namespace SharpChip8
{
	public partial class Window : Form
	{
		private readonly object myLock = new object();
		[DllImport("kernel32.dll", SetLastError = true)]
		[return: MarshalAs(UnmanagedType.Bool)]
		static extern bool AllocConsole();

		private float PixelSize = 1;
		private Chip.System ChipSystem;
		private Task runner;

		public Window()
		{
			InitializeComponent();
			this.KeyDown += SetKeyDown;
			this.KeyUp += SetKeyUp;
		}

		public void DrawOnScreen(float posx, float posy, float width, float height, Color color)
		{
			lock (myLock)
			{
				var graphics = Graphics.FromImage(this.Screen.Image);
				graphics.SmoothingMode = SmoothingMode.AntiAlias;
				graphics.FillRectangle(new SolidBrush(color), posx, posy, width, height);
			}
		}

		private void Draw(Pixel pixel)
		{
			DrawOnScreen(pixel.PosX * PixelSize, pixel.PosY * PixelSize, PixelSize, PixelSize, pixel.Color);
		}

		public void Draw()
		{
			foreach (Pixel pixel in this.ChipSystem.Cpu.Screen.Pixels)
			{
				Draw(pixel);
			}
			Program.RunOnUIThread(() =>
			{
				lock(myLock)
					this.Screen.Refresh();
			});
		}

		public void SetScreen(Chip.System chipSystem)
		{
			this.ChipSystem = chipSystem;
			ChipSystem.Cpu.Screen.Draw += Draw;
			SetPixelSize();
		}

		private void SetPixelSize()
		{
			lock (myLock)
			{
				float pxWidth = this.Screen.Width / ChipSystem.Cpu.Screen.Pixels.GetLength(0);
				float pxHeight = this.Screen.Height / ChipSystem.Cpu.Screen.Pixels.GetLength(1);
				PixelSize = Math.Min(pxHeight, pxWidth);
				this.Screen.Image = new Bitmap(this.Screen.Width, this.Screen.Height, PixelFormat.Format32bppArgb);
			}
		}

		private void Window_Resize(object sender, EventArgs e)
		{
			if (ChipSystem != null && ChipSystem.IsInit)
			{
				SetPixelSize();
				Draw();
			}
		}

		private void OpenFile(object sender, EventArgs e)
		{
			int size = -1;
			DialogResult result = openFileDialog1.ShowDialog();
			if (result == DialogResult.OK)
			{
				ChipSystem.LoadROM(openFileDialog1.FileName);
			}
		}

		private void StartButton_Click(object sender, EventArgs e)
		{
			if (runner != null)
				ChipSystem.Stop();
			runner = new Task(ChipSystem.Start);
			runner.ContinueWith(ExceptionHandler, TaskContinuationOptions.OnlyOnFaulted);
			runner.Start();
		}

		private void ExceptionHandler(Task task)
		{
			var exception = task.Exception;
			Console.WriteLine(exception);
			ChipSystem.Init();
		}

		private void Window_Load(object sender, EventArgs e)
		{
			AllocConsole();
		}

		private void SetKeyUp(object sender, KeyEventArgs e)
		{
			Key key = ChipSystem.KeyBoard.Keys.FirstOrDefault(x => x.Code == e.KeyCode);
			if (key != null)
			{
				key.IsPressed = false;
			}
		}

		// Handle the KeyDown event to print the type of character entered into the control.
		private void SetKeyDown(object sender, KeyEventArgs e)
		{
			Key key = ChipSystem.KeyBoard.Keys.FirstOrDefault(x => x.Code == e.KeyCode);
			if (key != null)
			{
				key.IsPressed = true;
			}
		}
	}
}
